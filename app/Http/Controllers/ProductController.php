<?php

namespace App\Http\Controllers;

use App\Helpers\ApiFormatter;
use Illuminate\Support\Facades\Http;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $response = Http::get('https://60c18de74f7e880017dbfd51.mockapi.io/api/v1/jabar-digital-services/product')->json();
        $rupiah = 0;
        foreach($response as $key => $value){
            if($key == 0){
                $rupiah = $this->convertCurrency($value['price'], 'USD', 'IDR') / $value['price'];
            }

            $response[$key]['IDR'] = $value['price'] * $rupiah;
        }
       
        return ApiFormatter::createApi(200, 'Success', $response);
    }

    public function index_aggregation(){
                
        $response = Http::get('https://60c18de74f7e880017dbfd51.mockapi.io/api/v1/jabar-digital-services/product')->json();
        $rupiah = 0;
        foreach($response as $key => $value){
            if($key == 0){
                $rupiah = $this->convertCurrency($value['price'], 'USD', 'IDR') / $value['price'];
            }

            $response[$key]['IDR'] = $value['price'] * $rupiah;
        }

        $sorting_insructions = [
            ['department', 'asc'],
            ['product', 'desc'],
            ['IDR', 'desc'],
        ];
        
        $response = collect($response);
        $response = $response->sortBy($sorting_insructions);

        return ApiFormatter::createApi(200, 'Success', $response);
    }

    private function convertCurrency($amount,$from_currency,$to_currency){
        $apikey = '4b9578cd3a35943362e9';

        $from_Currency = urlencode($from_currency);
        $to_Currency = urlencode($to_currency);
        $query =  "{$from_Currency}_{$to_Currency}";

        // change to the free URL if you're using the free version
        $json = file_get_contents("https://free.currconv.com/api/v7/convert?q={$query}&compact=ultra&apiKey={$apikey}");
        $obj = json_decode($json, true);

        $val = floatval($obj["$query"]);


        $total = $val * $amount;
        return number_format($total, 2, '.', '');
        }
}
