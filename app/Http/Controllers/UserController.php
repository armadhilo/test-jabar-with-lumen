<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Helpers\ApiFormatter;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function detail($id){
        $user = User::findOrFail($id);
        return ApiFormatter::createApi(200, 'Success', $user);
    }

    //
}
