<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Firebase\JWT\JWT;
use App\Models\User;

use App\Helpers\ApiFormatter;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function register(request $request)
    {

        $this->validate($request, [
            'nik' => 'required|unique:users,nik|digits:16',
            'role' => 'required|in:admin,user',
            'password' => 'required',
        ]);
       
        $request->merge(['password' => Hash::make($request->password)]);

        $data = User::create($request->all());
        return ApiFormatter::createApi(201, 'Success', $data);
        
    }
    
    public function login(request $request){
        $this->validate($request, [
            'nik' => 'required|exists:users,nik',
            'password' => 'required',
        ]);
        
        $user = User::where('nik', $request->nik)->first();
        if(!Hash::check($request->password, $user->password)){
            return ApiFormatter::createApi(401, 'NIK atau Password anda');
        }

        $payload = [
            "iat" => 1356999524,
            "nbf" => 1357000000,
            "exp" => time() + (60 * 1),
            'uid' => $user->id,
        ];

        $token = JWT::encode($payload, env('JWT_SECRET'), 'HS256');
        $user['token'] = $token;
        
        return ApiFormatter::createApi(200, 'Success', $user);
    }

   
}
