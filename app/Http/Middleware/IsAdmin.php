<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\ApiFormatter;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->role != 'admin'){
            return ApiFormatter::createApi(401, 'Unauthorized');
        }

        return $next($request);
    }
}
