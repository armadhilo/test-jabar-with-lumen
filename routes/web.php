<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'auth'], function () use ($router) {
    $router->post('register', 'AuthController@register');
    $router->get('login', 'AuthController@login');
});

$router->group(['prefix' => 'users', 'middleware' => 'auth'], function () use ($router) {
    $router->get('{id}', 'UserController@detail');
});

$router->group(['prefix' => 'products', 'middleware' => 'auth'], function () use ($router) {
    $router->get('/', 'ProductController@index');
});

$router->group(['prefix' => 'products', 'middleware' => ['auth', 'is_admin']], function () use ($router) {
    $router->get('/aggregation', 'ProductController@index_aggregation');
});